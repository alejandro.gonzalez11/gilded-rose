import { Item, GildedRose } from '@/gilded-rose';

describe('Gilded Rose', () => {
  /*it('afoo baja quality 2 cuando es 10', () => {
    const gildedRose = new GildedRose([new Item('foo', 0, 10)]);
    const items = gildedRose.updateQuality();
    expect(items[0].name).toBe('foo');
    expect(items[0].quality).toBe(8)
    expect(items[0].sellIn).toBe(-1)
  });

  it('afoo  quality no se modifica si arranca en 0', () => {
    const gildedRose = new GildedRose([new Item('foo', 0, 0)]);
    const items = gildedRose.updateQuality();
    expect(items[0].name).toBe('foo');
    expect(items[0].quality).toBe(0)
    expect(items[0].sellIn).toBe(-1)
  });

  it('aSulfuras, Hand of Ragnaros  ', () => {
    const gildedRose = new GildedRose([new Item('Sulfuras, Hand of Ragnaros', 0, 10)]);
    const items = gildedRose.updateQuality();
    expect(items[0].name).toBe('Sulfuras, Hand of Ragnaros');
    expect(items[0].quality).toBe(10)
    expect(items[0].sellIn).toBe(0)
  });

  it('aSulfuras, Hand of Ragnaros < 49 se suman 2 ', () => {
    const gildedRose = new GildedRose([new Item('Aged Brie', 0, 10)]);
    const items = gildedRose.updateQuality();
    expect(items[0].name).toBe('Aged Brie');
    expect(items[0].quality).toBe(12)
    expect(items[0].sellIn).toBe(-1)
  });

  it('aAged Brie quality > = 50 se mantiene en 50  ', () => {
    const gildedRose = new GildedRose([new Item('Aged Brie', 0, 50)]);
    const items = gildedRose.updateQuality();
    expect(items[0].name).toBe('Aged Brie');
    expect(items[0].quality).toBe(50)
    expect(items[0].sellIn).toBe(-1)
  });

  it('aAged Brie quality == 49 queda en 50  ', () => {
    const gildedRose = new GildedRose([new Item('Aged Brie', 0, 49)]);
    const items = gildedRose.updateQuality();
    expect(items[0].name).toBe('Aged Brie');
    expect(items[0].quality).toBe(50)
    expect(items[0].sellIn).toBe(-1)
  });

  it('aBackstage passes to a TAFKAL80ETC concert tiene que quedar en 0 si el quality en menor que 50 y el sellin menor igual que 0', () => {
    const gildedRose = new GildedRose([new Item('Backstage passes to a TAFKAL80ETC concert', 0, 10)]);
    const items = gildedRose.updateQuality();
    expect(items[0].name).toBe('Backstage passes to a TAFKAL80ETC concert');
    expect(items[0].quality).toBe(0)
    expect(items[0].sellIn).toBe(-1)
  });*/
});
