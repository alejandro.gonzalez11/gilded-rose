import {AGED_BRIE, BACKSTAGE, GildedRose, Item, SULFURAS} from "@/gilded-rose";

describe('giladed roses tests',  ()=>  {

    test('foo baja quality 2 cuando es 10', () => {
        const gildedRose = new GildedRose([new Item('foo', 0, 10)]);
        const items = gildedRose.updateQuality();
        expect(items[0].name).toBe('foo');
        expect(items[0].quality).toBe(8)
        expect(items[0].sellIn).toBe(-1)
    });

    test('foo  quality no se modifica si arranca en 0', () => {
        const gildedRose = new GildedRose([new Item('foo', 0, 0)]);
        const items = gildedRose.updateQuality();
        expect(items[0].name).toBe('foo');
        expect(items[0].quality).toBe(0)
        expect(items[0].sellIn).toBe(-1)
    });

    test('Sulfuras, Hand of Ragnaros  ', () => {
        const gildedRose = new GildedRose([new Item(SULFURAS, 0, 10)]);
        const items = gildedRose.updateQuality();
        expect(items[0].name).toBe(SULFURAS);
        expect(items[0].quality).toBe(10)
        expect(items[0].sellIn).toBe(0)
    });

    test('Sulfuras, Hand of Ragnaros < 49 se suman 2 ', () => {
        const gildedRose = new GildedRose([new Item(AGED_BRIE, 0, 10)]);
        const items = gildedRose.updateQuality();
        expect(items[0].name).toBe(AGED_BRIE);
        expect(items[0].quality).toBe(12)
        expect(items[0].sellIn).toBe(-1)
    });

    test('Aged Brie quality > = 50 se mantiene en 50  ', () => {
        const gildedRose = new GildedRose([new Item(AGED_BRIE, 0, 50)]);
        const items = gildedRose.updateQuality();
        expect(items[0].name).toBe(AGED_BRIE);
        expect(items[0].quality).toBe(50)
        expect(items[0].sellIn).toBe(-1)
    });

    test('Aged Brie quality == 49 queda en 50  ', () => {
        const gildedRose = new GildedRose([new Item(AGED_BRIE, 0, 49)]);
        const items = gildedRose.updateQuality();
        expect(items[0].name).toBe(AGED_BRIE);
        expect(items[0].quality).toBe(50)
        expect(items[0].sellIn).toBe(-1)
    });

    test('Backstage passes to a TAFKAL80ETC concert tiene que quedar en 0 si el quality en menor que 50 y el sellin menor igual que 0', () => {
        const gildedRose = new GildedRose([new Item(BACKSTAGE, 0, 10)]);
        const items = gildedRose.updateQuality();
        expect(items[0].name).toBe(BACKSTAGE);
        expect(items[0].quality).toBe(0)
        expect(items[0].sellIn).toBe(-1)
    });

    test('Backstage passes to a TAFKAL80ETC concert tiene que quedar en 0 si el quality en menor que 50 y el sellin menor igual que 0 2', () => {
        const gildedRose = new GildedRose([new Item(BACKSTAGE, 1, 48)]);
        const items = gildedRose.updateQuality();
        expect(items[0].name).toBe(BACKSTAGE);
        expect(items[0].quality).toBe(50)
        expect(items[0].sellIn).toBe(0)
    });
});