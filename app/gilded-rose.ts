export class Item {
    name: string;
    sellIn: number;
    quality: number;

    constructor(name, sellIn, quality) {
        this.name = name;
        this.sellIn = sellIn;
        this.quality = quality;
    }
}

export const AGED_BRIE = 'Aged Brie'
export const BACKSTAGE = 'Backstage passes to a TAFKAL80ETC concert'
export const SULFURAS = 'Sulfuras, Hand of Ragnaros'

export class GildedRose {
    items: Array<Item>;

    constructor(items = [] as Array<Item>) {
        this.items = items;
    }

    private isBackstageAndCanIncressQuality(itemNumber :number) {
        return this.items[itemNumber].name == BACKSTAGE && this.canIncressQuality(itemNumber)
    }

    private isAgedBrieAndCanIncressQuality(itemNumber :number) {
        return this.items[itemNumber].name == AGED_BRIE && this.canIncressQuality(itemNumber)
    }

    private canIncressQuality(itemNumber :number){
        return this.items[itemNumber].quality < 50
    }
    private canDecreaseQuality(itemNumber :number){
        return this.items[itemNumber].quality > 0
    }

    updateQuality() {
        for (let i = 0; i < this.items.length; i++) {
            if (this.items[i].name != SULFURAS) {
                this.items[i].sellIn--
                if (this.isBackstageAndCanIncressQuality(i) || (this.isAgedBrieAndCanIncressQuality(i))) {
                    this.items[i].quality++
                    if (this.items[i].sellIn < 11 && this.isBackstageAndCanIncressQuality(i)) {
                        this.items[i].quality++
                        if (this.items[i].sellIn < 6 && this.canIncressQuality(i)) {
                            this.items[i].quality++
                        }
                    }
                } else {
                    if (this.canDecreaseQuality(i)) {
                        this.items[i].quality--
                    }
                }
            }

            if (this.items[i].sellIn < 0) {
                if (this.items[i].name == AGED_BRIE) {
                    if (this.canIncressQuality(i)) {
                        this.items[i].quality++
                    }
                } else {
                    if (this.items[i].name != BACKSTAGE && this.items[i].quality > 0 && this.items[i].name != SULFURAS) {
                        this.items[i].quality--
                    } else {
                        this.items[i].quality = 0
                    }

                }
            }
        }

        return this.items;
    }
}
